# NYC schools

NYC High Schools code assessment repository.

## Here is the categories covered during coding test
1.            Followed best practices like writing Unit testing cases.
2.            External libraries like MBProgressHUD is using in the project via Cocoapods
3.            Use MVVM and Presenter pattern
4.            Horizontal and Vertical views if the phone is in rotation mode.
5.            Integrating Auto layouts, Threading and Memory management have been taken care.
